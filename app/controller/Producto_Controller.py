from app.resource import Rpta
from app.data_access.Producto_Data_Access import ProductoDataAccess
from app.models.Proveedor import Proveedor

def buscarMejorProveedor(idProd):
    res = {}
    rpta=Rpta()
    rpta._body= ProductoDataAccess.buscarMejorProveedor(idProd)[0]
    print(rpta._body)
    p,pp,pb = rpta._body
    print(pb.id_proveedor)
    aux= Proveedor.getProveedor(pb.id_proveedor)
    rpta._body=aux.nombre_proveedor
    rpta.setOk('Se creo correctamente')
    return rpta
