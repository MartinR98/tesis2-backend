from app.api import app
from flask_sqlalchemy import SQLAlchemy
from io import StringIO
from app.commons.credentials import user,password,host,port,dbName
  
def getSessionServer():
    #"""


    #ssh_user = 'inf245'
    #passwssh = "6CmINL2eRPo%baH"
    #passw= 'GiME4OI9'
    #
    #localhost = '127.0.0.1'

    
    #tunnel = sshtunnel.SSHTunnelForwarder(
    #        (host,22),
    #        ssh_password = passwssh,
    #        ssh_username=ssh_user,
    #        remote_bind_address=(localhost, 3306)
    #)
    #tunnel.start()
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://{}:{}@{}:{}/{}'.format(user,password,host, port ,dbName)
    #"""
    #app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:@localhost:3306/test'
    app.config['SQLALCHEMY_POOL_SIZE'] = 5
    app.config['SQLALCHEMY_POOL_TIMEOUT'] = 30
    app.config['SQLALCHEMY_POOL_RECYCLE'] = 31
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    db = SQLAlchemy(app)
    
    return db
