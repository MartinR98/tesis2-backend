from . import db
from sqlalchemy import func

class Pedido(db.Model):
    __tablename__="PEDIDO"
    id=db.Column('id',db.Integer,primary_key=True)
    descripcion=db.Column('descripcion',db.String(200))
    fecha_creacion=db.Column('fecha_creacion',db.DateTime,default=func.current_timestamp())
    precio_total=db.Column('precio_total',db.Float)