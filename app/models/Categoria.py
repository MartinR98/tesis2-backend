from . import db
from sqlalchemy import func

class Categoria(db.Model):
    __tablename__='CATEGORIA'
    id=db.Column('id',db.Integer,primary_key=True)
    nombre=db.Column('nombre',db.String(30))
    