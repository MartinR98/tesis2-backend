from . import db
from sqlalchemy import func
from app.models.Producto import Producto
from app.models.Proveedor import Proveedor

class ProductoxProveedor(db.Model):
    __tablename__='PRODUCTO_PROVEEDOR'
    id=db.Column('id',db.Integer,primary_key=True)
    id_producto=db.Column('id_producto',db.ForeignKey(Producto.id))
    id_proveedor=db.Column('id_proveedor',db.ForeignKey(Proveedor.id))
    stock=db.Column('stock',db.Integer)
    precio=db.Column('precio',db.Float)

    