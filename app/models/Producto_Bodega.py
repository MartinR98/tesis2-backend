from . import db
from sqlalchemy import func
from app.models.Producto import Producto
from app.models.Bodega import Bodega

class ProductoxBodega(db.Model):
    __tablename__='PRODUCTO_BODEGA'
    id=db.Column('id',db.Integer,primary_key=True)
    id_producto=db.Column('id_producto',db.ForeignKey(Producto.id))
    id_bodega=db.Column('id_bodega',db.ForeignKey(Bodega.id))
    stock=db.Column('stock',db.Integer)
    precio=db.Column('precio',db.Float)
    stock_minimo=db.Column('stock_minimo',db.Float)

    