from . import db
from sqlalchemy import func
from app.models.Producto import Producto
from app.models.Bodega import Bodega
from app.models.OrdenDeCompra import OrdenDeCompra

class DetallePedido(db.Model):
    __tablename__="DETALLEPEDIDO"
    id=db.Column('id',db.Integer,primary_key=True)
    id_producto=db.Column('id_producto',db.ForeignKey(Producto.id))
    id_bodega=db.Column('id_bodega',db.ForeignKey(Bodega.id))
    id_orden=db.Column('id_orden',db.ForeignKey(OrdenDeCompra.id))
    cantidad=db.Column('cantidad',db.Integer)