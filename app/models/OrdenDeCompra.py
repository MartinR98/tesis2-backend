from . import db
from sqlalchemy import func

class OrdenDeCompra(db.Model):
    __tablename__="ORDENDECOMPRA"
    id=db.Column('id',db.Integer,primary_key=True)
    descripcion=db.Column('descripcion',db.String(200))
    fecha_creacion=db.Column('fecha_creacion',db.DateTime,default=func.current_timestamp())
    precio=db.Column('precio',db.Float)
    estado=db.Column('estado',db.Integer)