from . import db
from sqlalchemy import func
from app.models.Producto_Bodega import ProductoxBodega
from app.models.Bodega import Bodega

class Kardex(db.Model):
    __tablename__='KARDEX'
    id=db.Column('id',db.Integer,primary_key=True)
    id_producto_bodega=db.Column('id_producto_bodega',db.ForeignKey(ProductoxBodega.id))
    id_bodega=db.Column('id_bodega',db.ForeignKey(Bodega.id))
    operacion=db.Column('operacion',db.Integer)