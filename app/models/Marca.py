from . import db
from sqlalchemy import func

class Marca(db.Model):
    __tablename__='MARCA'
    id=db.Column('id',db.Integer,primary_key=True)
    nombre=db.Column('nombre',db.String(30))
    