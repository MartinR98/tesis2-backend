from . import db
from sqlalchemy import func
from app.models.Proveedor import Proveedor
from app.models.Bodega import Bodega

class ProveedorxBodega(db.Model):
    __tablename__='PROVEEDOR_BODEGA'
    id=db.Column('id',db.Integer,primary_key=True)
    id_proveedor=db.Column('id_proveedor',db.ForeignKey(Proveedor.id))
    id_bodega=db.Column('id_bodega',db.ForeignKey(Bodega.id))
    calificacion=db.Column('calificacion',db.Integer)
    comentario=db.Column('comentario',db.String(200))

    