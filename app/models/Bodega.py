from . import db
from sqlalchemy import func

class Bodega(db.Model):
    __tablename__='BODEGA'
    id=db.Column('id',db.Integer,primary_key=True)
    nombre_bodega=db.Column('nombre_bodega',db.String(50))
    nombre_dueno=db.Column('nombre_dueno',db.String(50))
    email=db.Column('email',db.String(50))
    celular=db.Column('celular',db.String(15))
    ruc=db.Column('ruc',db.Integer)
    direccion=db.Column('direccion',db.String(50))
    usuario=db.Column('usuario',db.String(25))
    contrasena=db.Column('contrasena',db.String(25))
    stock_minimo=db.Column('stock_minimo',db.Integer)
