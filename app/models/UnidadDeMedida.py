from . import db
from sqlalchemy import func

class UnidadDeMedida(db.Model):
    __tablename__='UNIDAD_DE_MEDIDA'
    id=db.Column('id',db.Integer,primary_key=True)
    nombre=db.Column('nombre',db.String(30))
    