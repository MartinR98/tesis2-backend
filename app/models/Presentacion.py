from . import db
from sqlalchemy import func

class Presentacion(db.Model):
    __tablename__='PRESENTACION'
    id=db.Column('id',db.Integer,primary_key=True)
    nombre=db.Column('nombre',db.String(30))
    