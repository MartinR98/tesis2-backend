from . import db
from sqlalchemy import func
from app.models.Categoria import Categoria
from app.models.Marca import Marca
from app.models.UnidadDeMedida import UnidadDeMedida
from app.models.Presentacion import Presentacion

class Producto(db.Model):
    __tablename__='PRODUCTO'
    id=db.Column('id',db.Integer,primary_key=True)
    nombre=db.Column('nombre',db.String(40))
    id_categoria=db.Column('id_categoria',db.ForeignKey(Categoria.id))
    id_presentacion=db.Column('id_presentacion',db.ForeignKey(Presentacion.id))
    id_unidad=db.Column('id_unidad',db.ForeignKey(UnidadDeMedida.id))
    id_marca=db.Column('id_marca',db.ForeignKey(Marca.id))

    def toJSON(self):
        return {
            "id":self.id,
            "nombre":self.nombre
        }