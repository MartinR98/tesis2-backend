from app.models.Categoria import Categoria
from app.models.Producto import Producto
from app.models import db
class CategoriaDataAccess:
    def crearCategoria(nombreCat):
        try:
            categoria:Categoria
            categoria=Categoria(nombre=nombreCat)
            db.session.add(categoria)
            db.session.commit()
            db.session.flush()
            return categoria.id
        except Exception as e:
            raise e