from app.models.Producto import Producto
from app.models.Producto_Proveedor import ProductoxProveedor
from app.models.Proveedor_Bodega import ProveedorxBodega
from app.models.Proveedor import Proveedor
from app.models import db

class ProductoDataAccess:
    def buscarMejorProveedor(idProd):
        try:
            l=db.session.query(Producto,ProductoxProveedor,ProveedorxBodega).join(ProductoxProveedor).join(ProveedorxBodega,ProveedorxBodega.id_proveedor==ProductoxProveedor.id_proveedor).filter(idProd==ProductoxProveedor.id_producto).filter(Producto.id==idProd).filter(ProductoxProveedor.stock>0).order_by(ProveedorxBodega.calificacion.desc()).order_by(ProductoxProveedor.precio).all()
            db.session.commit()
            db.session.flush()
            return l
        except Exception as e:
            raise e