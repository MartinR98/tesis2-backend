from flask_restful import Resource
from . import Rpta
from flask import Flask, request
from app.controller.Categoria_Controller import crearCategoria

class Crear_categoria(Resource):
    def post(self):
        try:
            data = request.get_json()
            categoria = data['nombreCat']
            rpta=crearCategoria(categoria)
            return rpta.toJson()
        except Exception as e:
            answer=Rpta()
            answer.setError(str(e))#no se creo categoria
            return answer.toJson()
