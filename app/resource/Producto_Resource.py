from flask_restful import Resource
from . import Rpta
from flask import Flask, request
from app.controller.Producto_Controller import buscarMejorProveedor

class Buscar_mejor(Resource):
    def post(self):
        try:
            data = request.get_json()
            idProd = data['idProd']
            rpta=buscarMejorProveedor(idProd)
            return rpta.toJson()
        except Exception as e:
            answer=Rpta()
            answer.setError(str(e))#no se creo categoria
            return answer.toJson()
