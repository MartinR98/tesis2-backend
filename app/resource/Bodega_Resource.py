from flask_restful import Resource
from flask import Flask, request

class Listar_alumnos(Resource):
    def post(self):
        data = request.get_json()
        # VERIFICACION
        # SEGURIDAD
        rpta = dict()
        rpta['input'] = data 
        try:
            d = decode(data['access_token'])
            
            rpta['status'] = 203
            rpta['data'] = d

        except Exception as e:

            rpta['status'] = 402 
            rpta['data'] = 'error en decodificar| Excepcion : [ ' + str(e) + ' ]'
        finally:

            return rpta

class Crear_alumno(Resource):
    def post(self):
        data = request.get_json()
        alumnoDTO = AlumnoDTO(nombre=data['nombre'], apellido=data['apellido'])
        return crearAlumno(alumnoDTO)

class Obtener_alumno(Resource):
    def post(self):
        data = request.get_json()
        idAlumno = data['idAlumno']
        return obtenerAlumno(idAlumno)
