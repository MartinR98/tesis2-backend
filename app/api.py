from flask_restful import Api
from flask import Flask
from flask_cors import CORS, cross_origin
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

app = Flask(__name__)
api = Api(app)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

from app.models import db
from app.models.Bodega import Bodega
from app.models.Categoria import Categoria
from app.models.Pedido import Pedido
from app.models.Producto import Producto
from app.models.Proveedor import Proveedor
from app.models.OrdenDeCompra import OrdenDeCompra
from app.models.DetalleOrdenDeCompra import DetalleOrdenCompra
from app.models.DetallePedido import DetallePedido
from app.models.Producto_Bodega import ProductoxBodega
from app.models.Producto_Proveedor import ProductoxProveedor
from app.models.Proveedor_Bodega import ProveedorxBodega


migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)

from app.resource.Categoria_Resource import *
from app.resource.Producto_Resource import *
#from app.resource.AlumnoResource import *

api.add_resource(Buscar_mejor,'/api/producto/buscarMejorProv')
api.add_resource(Crear_categoria,'/api/categoria/crear')
#api.add_resource(UserResourceCreate, '/api/user/create')
